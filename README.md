# Gruppe 47 - OOprog Prosjekt 2017

#### Kontakt informasjon
*Bjarte Klyve Larsen*

Epost: `bjarteklarsen@gmail.com`

TLF: `95047857`

*Jørgen Hanssen*

Epost: `jorkiz97@gmail.com`

TLF: `90212403`

***
# Hvordan Kjøre programmet
For å kjøre/compile programmet må du enten sette opp include pathsene i IDE-en du bruker. Eller om du har `gcc/g++` og `make` Installert kan du kjøre `make run` 
Include paths er spesifisert i Makefile. Ta dette i betraktning når du skal compile programmet.
***

### Egne forutsetninger
- Kommentert ut linje i displayList funksjonen i List tools, vi har ikke bruk for å skrive hvor mange ting som er i listen på skjermen når vi kjører displayList