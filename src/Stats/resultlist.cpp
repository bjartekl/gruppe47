#include <ResultList.h>



namespace ooprog {

ResultList::ResultList() {

}

ResultList::ResultList(std::size_t id, std::string sport) {
  activeId = id;
  activeSport = sport;

  StartList start(activeId, activeSport);
  startList = start.getList();

  fileName = "data/exercise/OV_"+activeSport + std::to_string(activeId) + ".RES";

  readFromFile();
}

ResultList::~ResultList() {
}



void ResultList::remove() {
  if(Helper::removeFile(fileName)) {
    std::cout << "Removed resultList\n";
  }else {
    std::cout << "Could not remove resultList\n";
  }
}

void ResultList::parsePerson(std::ifstream &file) {
  std::size_t id;
  std::size_t startNumber;
  file >> id;
  file >> startNumber;

  std::cout << "\n\n";
  std::cout << "Start Number: " << startNumber << "\n";

  // Display the participant
  participants->displayParticipantById(id);
}

void ResultList::finishingNumber(std::size_t id) {
  Result res;
  res.id = id;
  std::cout << "Add finishing number on id: " << id << "\n";
  std::size_t pos = Helper::input.get(1, startList.size(), "Input must be number between 1 and participants");
  res.position = pos;
  res.status = FINISHED;
  std::string finishedTime;
  std::cout << "Enter time in format hh:mm:ss: ";
  Helper::input.get(finishedTime, "Input must be type string");
  res.time = finishedTime;
  list.push_back(res);
}
void ResultList::unfinished(std::size_t id, Status reason) {
  Result res;
  res.id = id;
  res.status = reason;
  list.push_back(res);
}

void ResultList::createResultList() {

  for (size_t i = 0; i < startList.size(); i++) {
    std::cout << "\n\n";
    std::cout << "Start number: " << i+1 << "\n";
    participants->displayParticipantById(startList.at(i));

    Menu menu("Finished");
    menu.registerMenuItem(1, 'N', "Add finishing number", std::bind(&ResultList::finishingNumber, this, startList.at(i)));
    menu.registerMenuItem(2, 'E', "Broke off", std::bind(&ResultList::unfinished, this, startList.at(i), BROKEOFF));
    menu.registerMenuItem(3, 'R', "no-show", std::bind(&ResultList::unfinished, this, startList.at(i), NOSHOW));
    menu.registerMenuItem(4, 'D', "Disqualified", std::bind(&ResultList::unfinished, this, startList.at(i), DISQUALIFIED));
    menu.display();
  }
  updateMedals();
  writeToFile();
}

void ResultList::display(){
  if(!Helper::fileExists(fileName)) {
    std::cout << fileName << "\n";
    std::cout << "Result list does not exist!\n";
  }else {

    for (size_t i = 0; i < list.size(); i++) {
      Result res = list.at(i);
      std::cout << "\n\n----------\n";
      std::cout << res.position << "\n";
      std::cout << res.time << "\n";
      switch (res.status) {
        case FINISHED:
          std::cout << "Finished\n";
        break;
        case BROKEOFF:
          std::cout << "Broke off\n";
        break;
        case NOSHOW:
          std::cout << "No-show\n";
        break;
        case DISQUALIFIED:
          std::cout << "Disqualified\n";
        break;
      }

      participants->displayParticipantById(res.id);
    }
  }
}

void ResultList::writeToFile() {
  // Make sure list is sorted before writing it to file
  std::sort(list.begin(), list.end());
  std::ofstream resFile(fileName);
  resFile << list.size() << "\n";
  for (size_t i = 0; i < list.size(); i++) {
    Result res = list.at(i);
    resFile << res.position << " " << res.id << " "
            << res.time << " "<< res.status << "\n";
  }
}


void ResultList::updateMedals() {
  std::vector<std::string> nationList;
  stats.importNations(nationList);
  Result res;
  res = list.at(0);
  std::size_t gold = res.id;
  std::string goldNation = participants->getParticipantNation(gold);
  res = list.at(1);
  std::size_t silver = res.id;
  std::string silverNation = participants->getParticipantNation(silver);
  res = list.at(2);
  std::size_t bronze = res.id;
  std::string bronzeNation = participants->getParticipantNation(bronze);
  for (size_t i = 0; i < nationList.size(); i++) {
    if(goldNation == nationList.at(i)) {
      medals->addGold(i);
    }
    if(silverNation == nationList.at(i)) {
      medals->addSilver(i);
    }
    if(bronzeNation == nationList.at(i)) {
      medals->addBronze(i);
    }
  }
}


void ResultList::readFromFile() {
  std::ifstream resFile(fileName);
  std::size_t num = 0;
  resFile >> num;
  for (size_t i = 0; i < num; i++) {
    Result res;
    std::size_t status = 0;
    resFile >> res.position >> res.id >> res.time;
    resFile >> status;
    res.status = static_cast<Status>(status);
    list.push_back(res);
  }
  std::sort(list.begin(), list.end());
}

};
