#include <StartList.h>

namespace ooprog {

StartList::StartList() {

}

StartList::StartList(std::size_t id, std::string sport) {
  activeId = id;
  activeSport = sport;
  fileName = "data/exercise/OV_"+activeSport + std::to_string(activeId) + ".STA";
  parseFile();
}

StartList::~StartList() {
}


void StartList::addParticipantsToList() {
  std::cout << "Number of participants to add (1-" << MAXPARTICIPANTSINLISTS << ": ";
  std::size_t num = Helper::input.get(1, MAXPARTICIPANTSINLISTS, "Input must be type number between 1 and 1000");

  Participant *par{nullptr};
  bool inList = false;
  for (size_t i = 0; i < num; i++) {
    par = nullptr;
    inList = true;
    std::size_t id = 0;

    while (!par || inList) {
      std::cout << "Participant id: ";
      Helper::input.get(id, "Input must be type number");
      if(Helper::indexOf(list, id) != -1) {
        inList = true;
        std::cout << "Participant is already in the list\n";
      }else {
        inList = false;
        par = participants->getParticipant(id);
        if(!par) {
          std::cout << "Please provide a valid participant ID\n";
        }
      }

    }
    list.push_back(id);
    par->display();
  }
  writeToFile();
}

void StartList::createStartList() {
  if (!fileExists)
    addParticipantsToList();
  else std::cout << activeSport << " already has a list for this exercise";
}


void StartList::writeToFile() {
  std::ofstream sta(fileName.c_str());
  std::size_t listSize = list.size();
  sta << listSize << "\n";
  for (size_t i = 0; i < listSize; i++) {
    sta << list.at(i) << '\n';
  }
}

void StartList::parseFile() {
  std::ifstream sta(fileName);
  std::size_t num = 0;

  fileExists = sta.good();
  sta >> num;
  std::cout << "Number of participants: "<<num<<"\n";
  std::size_t id;
  for (size_t i = 0; i < num; i++) {
    sta >> id;
    list.push_back(id);
  }
}

void StartList::editStartList() {
  if (fileExists) {
    display();
    while(displayMenu) {
      showEditMenu();
    }
  } else std::cout << "No lists are registered for this exercise";
}
void StartList::exitMenu() {
  displayMenu = false;
}

void StartList::editParticipantList() {
    display();
    std::cout << "Enter participant start number: ";
    std::size_t num = Helper::input.get(1, list.size(), "Input must be between 1 and list size");
    std::size_t tmp = list.at(num-1);

    std::cout << "New start position: ";
    std::size_t newPos = Helper::input.get(1, list.size(), "Input must be between 1 and list size");
    std::size_t removePos = (num < newPos) ? num - 1 : num;
    std::size_t movePos = (num > newPos) ? newPos - 1 : newPos;


    list.insert(list.begin() + movePos, tmp);
    std::cout << "Removing from pos: " << removePos << "\n";
    list.erase(list.begin() + removePos);

    writeToFile();
}

void StartList::removeParticipant() {
  display();
  std::cout << "Enter participant start number to remove: ";
  std::size_t num = Helper::input.get(1, list.size(), "Input must be between 1 and list size");
  list.erase(list.begin() + num-1);

  writeToFile();
}

void StartList::showEditMenu() {
  Menu menu("Edit StartList");
  menu.registerMenuItem(1, 'N', "Add Participants", std::bind(&StartList::addParticipantsToList, this));
  menu.registerMenuItem(2, 'E', "Edit participant start nr", std::bind(&StartList::editParticipantList, this));
  menu.registerMenuItem(3, 'R', "Remove Participant", std::bind(&StartList::removeParticipant, this));
  menu.registerMenuItem(4, 'X', "Exit", std::bind(&StartList::exitMenu, this));
  menu.display();
}


std::vector<std::size_t> StartList::getList() {
  return list;
}



void StartList::remove() {
  if(Helper::removeFile(fileName)) {
    std::cout << "Removed resultList\n";
  }else {
    std::cout << "Could not remove resultList\n";
  }
}



void StartList::display(){
  if (fileExists) {
    for (size_t i = 0; i < list.size(); i++) {
      std::cout << "\n\n";
      std::cout << "Start number: " << i+1 << "\n";
      participants->displayParticipantById(list.at(i));
    }
  } else std::cout << "No lists are registered for this exercise";
}

};
