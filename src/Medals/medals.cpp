#include <Medals.h>


namespace ooprog {

Medals::Medals() {
}

Medals::~Medals() {
}

void Medals::addNationMedals(std::ifstream &medalFile) {
  medals med;
  medalFile >> med.gold >> med.silver >> med.bronze;
  medalslist.push_back(med);
}
void Medals::readFromFile() {
  std::ifstream medalFile("data/medals.dta");
  for (size_t i = 0; i < nationlist.size(); i++) {
    addNationMedals(medalFile);
  }
}

void Medals::display() {
  stats.importNations(nationlist);
  readFromFile();
  for (size_t i = 0; i < nationlist.size(); i++) {
    std::cout << nationlist.at(i) << " ";
    medalslist.at(i).display();
  }
  medalslist.clear();
}


void Medals::addGold(std::size_t pos) {
  medals med = medalslist.at(pos);
  med.gold++;
  writeToFile();
}
void Medals::addSilver(std::size_t pos) {
  medals med = medalslist.at(pos);
  med.silver++;
  writeToFile();
}
void Medals::addBronze(std::size_t pos) {
  medals med = medalslist.at(pos);
  med.bronze++;
  writeToFile();
}

void Medals::writeToFile() {
  std::ofstream medalFile("data/medals.dta", std::ios::trunc);
  for (size_t i = 0; i < medalslist.size(); i++) {
    medals med = medalslist.at(i);
    medalFile << med.gold << " " << med.silver << " " << med.bronze << "\n";
  }
}
void Medals::createMenu(Menu &menu) {
  menu.registerMenuItem(5, 'M', "Medaljeoversikt", std::bind(&Medals::display, this));
}

};
