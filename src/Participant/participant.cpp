#include <Participant.h>


namespace ooprog {

enum Sex : std::size_t {male, female};

Participant::Participant(std::size_t id, std::string nationName) : NumElement(id) {
  nation = nationName;
  participantId = id;
  std::cout << "Athlete full name: ";
  Helper::input.getline(name, "Illegal, input");

  std::cout << "Athlete sex\n"
            << "1. Male\n"
            << "2. Female\n";
  std::size_t num = Helper::input.get(1,2, "Input must be between 1. Male and 2. Female");
  if(num == 1)
    sex = male;
  else
    sex = female;
}

Participant::Participant(std::size_t id, std::ifstream &file) : NumElement(id) {
  participantId = id;
  std::getline(file, nation, '$');
  std::getline(file, name, '$');
  std::size_t gender;
  file >> gender;
  if(gender == 1)
    sex = male;
  else
    sex = female;
  file.ignore();
}


void Participant::edit() {
  std::cin.ignore();
  std::cout << "Athlete name ["<<name<<"]: ";
  std::string newName;
  Helper::input.getline(newName, "Illegal, input");

   if(newName.length() > 0)
    name = newName;

  std::cout << "Athlete sex["<<((sex == male)?"Male":"Female")
            << "]\n1. Male\n"
            << "2. Female\n";

  std::size_t num = Helper::input.get(1,2, "Input must be between 1. Male and 2. Female");

  if(num == 1)
    sex = male;
  else
    sex = female;
}

Participant::~Participant() {
}


void Participant::displayIfNation(std::string nationName) {
  if(nationName == nation)
    display();
}

void Participant::display() {
  std::cout << "----------\n";
  std::cout << "ID: \t"     << participantId
            << "\nNation\t" << nation
            << "\nName: \t" << name
            << "\nSex: \t"  << ((sex == male)?"Male":"Female");
  std::cout << "\n----------\n";
}

std::string Participant::getNation() {
  return nation;
}

void Participant::toFile(std::ofstream &file) {
  file << participantId << " "
       << nation << "$"
       << name << "$"
       << ((sex == male)?"1":"2") << "\n";
}


};
