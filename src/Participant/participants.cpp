#include <Participants.h>
#include <ListTool2B.h>


namespace ooprog {


Participants::Participants() {
  participants = new List(Sorted);
}

Participants::~Participants() {
  delete participants;
}

void Participants::display() {
}

void Participants::registerParticipant() {
  std::cout << "Enter nations 3 letter Country-code to register participant \n";
  std::cout << "Country-code: ";
  std::string nationName;
  Helper::input.getline(nationName, "Invalid nation name");

  if(nations->nationExists(nationName)) {
    std::cout << "Enter athlete id: ";
    std::size_t id;
    Helper::input.get(id, "Invalid ID"); std::cin.ignore();
    if (!participants->inList(id)){
      participants->add(new Participant(id, nationName));
      nations->plusParticipantCount(nationName);
      writeFile();
    }else {
      std::cout << "Athlete already exists! \n";
    }
  }else {
    std::cout << "Nation does not exist! \n";
  }
}

void Participants::editParticipant() {
  std::cout << "Enter participant ID: ";
  std::size_t id;
  Helper::input.get(id, "Invalid ID");
  if(participants->inList(id)) {
    Participant *par = static_cast<Participant*>(participants->remove(id));
    par->edit();
    participants->add(par);
    writeFile();
  }else {
    std::cout << "\n\tParticipant does not exist \n";
  }
}

void Participants::displayNationAthletes(std::string nation) {
  for (size_t i = 1; i <= participants->noOfElements(); i++) {
    Participant *par = static_cast<Participant*>(participants->removeNo(i));
    par->displayIfNation(nation);
    participants->add(par);
  }
}

Participant *Participants::getParticipant(std::size_t id) {
  Participant *par = static_cast<Participant*>(participants->remove(id));
  if(par)
    participants->add(par);
  return par;
}

void Participants::displayParticipants() {
  participants->displayList();
}


void Participants::displayParticipant() {
  std::size_t id;

  std::cout << "Athlete id: ";
  Helper::input.get(id, "Invalid number!");
  displayParticipantById(id);
}

void Participants::displayParticipantById(std::size_t id) {
  if (Participant* participant = static_cast<Participant*>(participants->remove(id))) {
    participant->display();
    participants->add(participant);
  } else std::cout << "\n\tParticipant does not exist \n";
}

std::string Participants::getParticipantNation(std::size_t id) {
  if (Participant* participant = static_cast<Participant*>(participants->remove(id))) {
    participant->getNation();
    participants->add(participant);
  } else std::cout << "\n\tParticipant does not exist \n";
}


void Participants::writeFile() {
  std::ofstream file("./data/athletes.dta", std::ios::trunc);

  if (file) {
    std::size_t num = participants->noOfElements();
    file << num << "\n";

    for (size_t i = 1; i <= num; ++i) {
      Participant *par = static_cast<Participant*>(participants->removeNo(i));
      par->toFile(file);
      participants->add(par);
    }
  } else {
    std::cout << "\n\tCould not write to file athletes.dta!" << std::endl;
  }
}

void Participants::readFile() {
  std::ifstream file("./data/athletes.dta");

  if (file) {
    size_t num;
    file >> num; file.ignore();
    for (size_t i = 0; i < num; ++i) {
      std::size_t id;
      file >> id; file.ignore();
      participants->add(new Participant(id, file));
    }

  } else {
    std::cout << "\n\tCould not read from file athletes.dta!" << std::endl;
  }
}




void Participants::createMenu(Menu &menu) {
  ooprog::MenuItem *deltagere = menu.registerMenuItem(2, 'D', "Deltagere");
    deltagere->registerMenuItem(1, 'N', "Registrer en ny deltager", std::bind(&Participants::registerParticipant, this));
    deltagere->registerMenuItem(2, 'E', "Endre en deltager", std::bind(&Participants::editParticipant, this));
    deltagere->registerMenuItem(3, 'A', "Skriv hoveddataene om alle deltagere", std::bind(&Participants::displayParticipants, this));
    deltagere->registerMenuItem(4, 'S', "Skriv alle data om en gitt deltager", std::bind(&Participants::displayParticipant, this));
    //deltagere->registerMenuItem(5, 'Q', "Tilbake", deltagere->back);
}

};
