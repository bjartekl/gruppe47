#include <Exercise.h>


namespace ooprog {

Exercise::Exercise(size_t ID) : NumElement(ID) {
  std::cout << " Exercise name: ";
    Helper::input.getline(name, "Invalid name!");

  std::cout << " Set time (hhmm): ";
    Helper::input.get(time, "Invalid number!"); std::cin.ignore();

  std::cout << " Set date (yyyymmdd): ";
    Helper::input.get(date, "Invalid number!"); std::cin.ignore();
}


Exercise::Exercise(size_t ID, std::ifstream & fromFile) : NumElement(ID) {
  std::getline(fromFile, name, '$');

  fromFile >> time >> date; fromFile.ignore();
}


Exercise::~Exercise() {
}


void Exercise::display() {
  std::cout << "\n " << name << '\n';
  std::cout << "  ID:                     " << number << std::endl;
  std::cout << "  Time:                   " << time << std::endl;
  std::cout << "  Date:                   " << date << std::endl;
  //std::cout << "  Number of participants: " << numParticipants << std::endl;
}


void Exercise::createMenu(Menu &menu) {

}

void Exercise::toFile(std::ofstream & toFile) {
  toFile << '\t' << number << ' '
         << name << '$'
         << time << ' '
         << date << '\n';
}

void Exercise::edit() {
  char choice;

  std::cout << "\nCurrent name: " << name << std::endl;
  std::cout << " Edit? ('Y' = yes): ";
    Helper::input.get(choice, "\n\t1 character only!"); std::cin.ignore();
    if (std::toupper(choice) == 'Y') {
      std::cout << "New name: ";
      Helper::input.getline(name, "Invalid name!");
    }
  std::cout << "\nCurrent time: " << time << std::endl;
  std::cout << " Edit? ('Y' = yes): ";
    Helper::input.get(choice, "\n\t1 character only!"); std::cin.ignore();
    if (std::toupper(choice) == 'Y') {
      std::cout << "Set new time: ";
      Helper::input.get(time, "Invalid number!");
    }
  std::cout << "\nCurrent date: " << date << std::endl;
  std::cout << " Edit? ('Y' = yes): ";
    Helper::input.get(choice, "\n\t1 character only!");
    if (std::toupper(choice) == 'Y') {
      std::cout << "Set new date: ";
      Helper::input.get(date, "Invalid number!");
    }
}

};
