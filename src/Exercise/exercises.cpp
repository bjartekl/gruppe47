#include <Exercises.h>

namespace ooprog {


Exercises::Exercises() {
  activeSport = "";
}


Exercises::~Exercises() {
}


void Exercises::display() {
  sport->displayExercises();
}


void Exercises::registerExercise() {
  getId();

  sport->registerExercise(activeId);
  sports->writeFile();
}


void Exercises::getName() {
  std::cout << "Enter sport's name: ";
  Helper::input.getline(activeSport, "Invalid input");

  if (sport = sports->getSport(activeSport))
    displaySubMenu();
  else
    std::cout << "Sport does not exist!\n";
}


void Exercises::getId() {
  std::cout << "Enter exercise id: ";
  Helper::input.get(activeId, "Invalid input"); std::cin.ignore();
}


void Exercises::reset() {
  activeId = 0;
  activeSport = "";
}


void Exercises::editExercise() {
  size_t ID;

  std::cout << "\nEnter exercise ID: ";
    Helper::input.get(ID, "\n\tInvalid number!");

  if (sport->editExercise(ID))
      sports->writeFile();
}


void Exercises::writeStartList() {
  StartList start(activeId, activeSport);
  start.display();
}
void Exercises::removeStartList() {
  StartList start(activeId, activeSport);
  start.remove();
}
void Exercises::createStartList() {
  StartList start(activeId, activeSport);
  start.createStartList();
}
void Exercises::editStartList() {
  StartList start(activeId, activeSport);
  start.editStartList();
}


void Exercises::writeResultList() {
  ResultList res(activeId, activeSport);
  res.display();
}
void Exercises::createResultList() {
  ResultList res(activeId, activeSport);
  res.createResultList();
}
void Exercises::removeResultList() {
  ResultList res(activeId, activeSport);
  res.remove();
}


void Exercises::displaySubMenu() {
  char title[STRLEN];
    strcpy(title, activeSport.c_str());

  Menu menu(title);
  menu.registerMenuItem(1, 'N', "Registrer en ny ovelse ", std::bind(&Exercises::registerExercise, this));
  menu.registerMenuItem(2, 'E', "Endre en ovelse", std::bind(&Exercises::editExercise, this));
  menu.registerMenuItem(3, 'F', "Fjerne/slette en ovelse", std::bind(&Exercises::removeExercise, this));
  menu.registerMenuItem(4, 'A', "Skriv hoveddataene om alle ovelser",std::bind(&Exercises::display, this));
  //
  ooprog::MenuItem *start   = menu.registerMenuItem(5, 'L', "Deltager / Startliste", std::bind(&Exercises::getId, this));
    start->registerMenuItem(1, 'S', "Skriv deltager-/startliste", std::bind(&Exercises::writeStartList, this));
    start->registerMenuItem(2, 'N', "Ny deltager-/startliste", std::bind(&Exercises::createStartList, this));
    start->registerMenuItem(3, 'E', "Endre deltager-/startliste", std::bind(&Exercises::editStartList, this));
    start->registerMenuItem(4, 'F', "Fjerne/slette deltager-/startliste", std::bind(&Exercises::removeStartList, this));

  ooprog::MenuItem *result  = menu.registerMenuItem(6, 'R', "Deltager / resultatliste", std::bind(&Exercises::getId, this));
    result->registerMenuItem(1, 'S', "Skriv resultatliste", std::bind(&Exercises::writeResultList, this));
    result->registerMenuItem(2, 'N', "Ny resultatliste", std::bind(&Exercises::createResultList, this));
    result->registerMenuItem(3, 'F', "Fjerne/slette resultatliste", std::bind(&Exercises::removeResultList, this));

  menu.display();
}


void Exercises::createMenu(Menu &menu) {
  ooprog::MenuItem *ovelse = menu.registerMenuItem(4, 'O', "Ovelse", std::bind(&Exercises::getName, this));
}

void Exercises::removeExercise() {
  getId();

  if (Exercise* tmp = sport->removeExercise(activeId)) {
    StartList start(activeId, activeSport);
    ResultList res(activeId, activeSport);

    start.remove();
    res.remove();

    delete tmp; tmp = nullptr;
  } else std::cout << "\n\tThere are no such exercises!";

  sports->writeFile();
}

}
