#include <Nations.h>
#include <Participants.h>

extern ooprog::Participants *participants;

namespace ooprog {

Nations::Nations() {
  nations = new List(Sorted);
}


Nations::~Nations() {
  delete nations;
}


void Nations::display(){
  nations->displayList();
}


void Nations::createMenu(Menu &menu) {
  ooprog::MenuItem *nasjoner = menu.registerMenuItem(1, 'N', "Nations");
    nasjoner->registerMenuItem(1, 'N', "Register new nation", std::bind(&Nations::registerNation, this));
    nasjoner->registerMenuItem(2, 'E', "Edit nation", std::bind(&Nations::editNation, this));
    nasjoner->registerMenuItem(3, 'A', "Skriv hoveddataene om alle nasjoner", std::bind(&Nations::display, this));
    nasjoner->registerMenuItem(4, 'T', "Skriv en nasjons deltagertropp", std::bind(&Nations::displayNationAthletes, this));
    nasjoner->registerMenuItem(5, 'S', "Skriv alle data om en gitt nasjon", std::bind(&Nations::displayAll, this));
    //nasjoner->registerMenuItem(6, 'Q', "Tilbake", nasjoner->back);
}


void Nations::displayNation(std::string nationName) {
  Nation *nat = static_cast<Nation*>(nations->remove(nationName.c_str()));
  nat->display();
  nations->add(nat);
}

void Nations::displayAll() {
  std::cout << "Enter nations 3 letter Country-code to register participant \n";
  std::cout << "Country-code: ";
  std::string nationName;
  Helper::input.getline(nationName, "Invalid nation name");
  if(nationExists(nationName)) {
    displayNation(nationName);
    participants->displayNationAthletes(nationName);
  }else {
    std::cout << "Nation does not exist!";
  }
}

void Nations::displayNationAthletes() {
  std::cout << "Enter nations 3 letter Country-code to register participant \n";
  std::cout << "Country-code: ";
  std::string nationName;
  Helper::input.getline(nationName, "Invalid nation name");

  if(nationExists(nationName)) {
    participants->displayNationAthletes(nationName);
  }else {
    std::cout << "Nation does not exist!";
  }
}

// also updates stats
void Nations::writeFile() {
  std::ofstream toFile("./data/nations.dta", std::ios::trunc);

  if (toFile) {
    Nation* nation;
    size_t numNations = nations->noOfElements();
    std::vector<std::string> updatedNationlist;

    toFile << numNations << "\n";

    for (size_t i = 1; i <= numNations; ++i) {
      nation = static_cast<Nation*>(nations->removeNo(i));
      std::string nationName = nation->getId();
      updatedNationlist.push_back(nationName);
      nation->toFile(toFile);
      nations->add(nation);
    }
    stats.update(updatedNationlist);
  } else std::cout << "\n\tCould not write to file nations.dta!\n";
}


void Nations::readFile() {
  std::ifstream fromFile("./data/nations.dta");

  if (fromFile) {
    char ID[3];
    size_t numNations;
    fromFile >> numNations; fromFile.ignore();

    for (size_t i = 0; i < numNations; ++i) {
      fromFile >> ID; fromFile.ignore();
      nations->add( new Nation(ID, fromFile) );
    }
    writeFile(); // just to initialize stats
  } else std::cout << "\n\tCould not read from file nations.dta!\n";
}


void Nations::registerNation() {
  if (nations->noOfElements() < 200) {
    char ID[3];

    std::cout << "\nEnter an unique ID (3 characters): ";
      Helper::input.get(ID, "\n\tID must be 3 characters");
    std::cin.ignore();

    if (Nation* nation = static_cast<Nation*>(nations->remove(ID))) {
      std::cout << "\n\tID already registered for nation: " << nation->getName() << "\n";
      nations->add(nation);
    } else {
      nations->add( new Nation(ID) );
      writeFile();
    }
  }
}

void Nations::editNation() {
  char ID[3];

  std::cout << "\nEnter nation ID: ";
    Helper::input.get(ID, "\n\tID must be 3 characters");

  if (Nation* nation = static_cast<Nation*>(nations->remove(ID))) {
    nation->edit();
    nations->add(nation);
    writeFile();
  } else std::cout << "\n\tID not registered\n";
}


bool Nations::nationExists(std::string nationName) {
  return nations->inList(nationName.c_str());
}


void Nations::plusParticipantCount(std::string nationName) {
  Nation* nation = static_cast<Nation*>(nations->remove(nationName.c_str()));
    nation->plusParticipantCount();
  nations->add(nation);
  writeFile();
}


};
