#include <Nation.h>


namespace ooprog {

Nation::Nation(char* ID) : TextElement(ID) {
  numParticipants = 0;

  std::cout << " Nation name: ";
    Helper::input.getline(name, "Invalid name!");

  std::cout << " Contact's name: ";
    Helper::input.getline(contactName, "Invalid name!");

  std::cout << " Contact's cellphone: ";
    Helper::input.get(contactCell, "Invalid number!"); std::cin.ignore();
}


Nation::Nation(char* ID, std::ifstream & fromFile) : TextElement(ID) {
  std::getline(fromFile, name, '$');
  fromFile >> numParticipants;
  std::getline(fromFile, contactName, '$');
  fromFile >> contactCell;
  fromFile.ignore();
}


Nation::~Nation() {
}


void Nation::display() {
  std::cout << '\n' << name << '\n';
  std::cout << " ID:                     " << text << std::endl;
  std::cout << " Number of participants: " << numParticipants << std::endl;
}


void Nation::toFile(std::ofstream &toFile) {
  toFile  << text << " "
          << name << "$"
          << numParticipants
          << contactName << "$"
          << contactCell << "\n";
}


std::string Nation::getName() {
  return name;
}

std::string Nation::getId() {
  return text;
}


void Nation::edit() {
  char choice;

  std::cout << "\nCurrent name: " << name << std::endl;
  std::cout << " Edit? ('Y' = yes): ";
    Helper::input.get(choice, "\n\t1 character only!"); std::cin.ignore();
    if (std::toupper(choice) == 'Y') {
      std::cout << "New name: ";
      Helper::input.getline(name, "Invalid name!");
    }
  std::cout << "\nCurrent contact name: " << contactName << std::endl;
  std::cout << " Edit? ('Y' = yes): ";
    Helper::input.get(choice, "\n\t1 character only!"); std::cin.ignore();
    if (std::toupper(choice) == 'Y') {
      std::cout << "New name: ";
      Helper::input.getline(contactName, "Invalid name!");
    }
  std::cout << "\nCurrent contact cellphone: " << contactCell << std::endl;
  std::cout << " Edit? ('Y' = yes): ";
    Helper::input.get(choice, "\n\t1 character only!");
    if (std::toupper(choice) == 'Y') {
      std::cout << "New cellphone: ";
      Helper::input.get(contactCell, "Invalid number!");
    }
}

void Nation::plusParticipantCount() {
  ++numParticipants;
}

};
