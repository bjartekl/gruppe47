#include <Menu.h>
#include <MenuItem.h>
#include <Helper.h>
#include <string>

#ifdef _WIN32
  #include <stdlib.h>
#endif

namespace ooprog {

Menu::Menu(char *n, Menu *ptr) {
  items = new List(Sorted);
  name = new char[std::strlen(n) + 1];
  std::strcpy(name, n);
  parent = ptr;
}


Menu::~Menu() {
  MenuItem *tmp;
  delete items;
  delete name;
}

MenuItem * Menu::registerMenuItem(std::size_t id, char key, char *text, std::function<void()>fn){
  Menu *par = this;
  if(parent)
    par = parent;
  MenuItem *tmp = new MenuItem(id, key, text, par, fn);
  items->add(tmp);
  return tmp;
}

void Menu::getInput() {
  std::cout << "Enter command: ";
  char input;
  Helper::input.get(input, "Input must be a single char");
  std::cin.ignore();
  input = std::toupper(input);
  parseInput(input);
}

void Menu::parseInput(char key) const {
  for (size_t i = 1; i <= items->noOfElements(); ++i) {
    MenuItem *tmp = static_cast<MenuItem*>(items->removeNo(i));
    tmp->callIfMatch(key);
    items->add(tmp);
  }
}

void Menu::display() {
  //std::cout << "\x1B[2J\x1B[H";
  #ifdef _WIN32
    system("cls")
  #endif

  std::cout << "\n***************************************\n";
  std::cout << name;
  std::cout << "\n***************************************\n\n";
  items->displayList();
  // for (size_t i = 1; i <= items->noOfElements(); ++i) {
  //   MenuItem *tmp = static_cast<MenuItem*>(items->removeNo(i));
  //   tmp->display();
  //   items->add(tmp);
  // }
  std::cout << "\n\n***************************************\n";

  getInput();
}

};
