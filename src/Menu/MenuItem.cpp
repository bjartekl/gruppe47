#include <MenuItem.h>
#include <Menu.h>

namespace ooprog {

MenuItem::MenuItem(std::size_t id, char key, char *text, Menu *prev, std::function<void()>fn) : NumElement(id) {
  menuId = id;
  menuKey = key;
  menuText = new char[std::strlen(text) + 1];
  std::strcpy(menuText, text);
  previous = prev;
  callback = fn;
  subMenu = new Menu(text, previous);
}


MenuItem::~MenuItem() {
  delete subMenu;
  delete menuText;
}

void MenuItem::callIfMatch(char key) {

  if(key == menuKey) {
    if(callback)
      callback();

    if(hasSubMenu) {
      subMenu->display();
    }
  }
}
void MenuItem::display() {
  std::cout << "\t" << menuKey << " - " << menuText << "\n";
}

MenuItem* MenuItem::registerMenuItem(std::size_t subId, char key, char *text, std::function<void()>fn) {
  if(!hasSubMenu)
    hasSubMenu = true;
  return subMenu->registerMenuItem(subId, key, text, fn);
}

std::size_t MenuItem::getMenuId() {
  return menuId;
}



};
