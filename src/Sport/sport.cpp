#include <Sport.h>

namespace ooprog {

enum TP : std::size_t {time, points};

Sport::Sport(std::string name) : TextElement(name.c_str()) {
  exercises = new List(Sorted);
  char c;

  std::cout << "Is the sport based on t(ime) or p(oints)?: ";

  while (c != 'T' && c != 'P') {
    Helper::input.get(c, "\n\tInput must be 1 character");
    c = std::toupper(c);
  }

  switch (c) {
    case 'T': {
      tp = time; break;
    }
    case 'P': {
      tp = points; break;
    }
  }
}

Sport::Sport(std::string name, std::ifstream & fromFile) : TextElement(name.c_str()) {
  size_t numEx, tmp_tp, exID;
  exercises = new List(Sorted);

  fromFile >> tmp_tp;
    tp = (tmp_tp) ? points : time;
  fromFile >>  numEx; fromFile.ignore(0);

  for (size_t i = 0; i < numEx; ++i) {
    fromFile >> exID; fromFile.ignore();
    exercises->add( new Exercise(exID, fromFile) );
  }
}


Sport::~Sport() {
  delete exercises;
}


void Sport::display() {
  std::string s;

  switch (tp) {
    case 0: {
      s = "time"; break;
    }
    case 1: {
      s = "points"; break;
    }
  }

  std::cout << '\n' << text << '\n';
  std::cout << " Score based on:      " << s << std::endl;
  std::cout << " Number of exercises: " << exercises->noOfElements() << std::endl;
}


void Sport::displayExercises() {
  if (exercises->noOfElements() > 0) {
    std::cout << "\n\n Exercises:\n";
      exercises->displayList();
  }
}


void Sport::displayAll() {
  display(); displayExercises();
}


void Sport::toFile(std::ofstream &toFile) {
  Exercise* ex;
  size_t numEx = exercises->noOfElements();

  toFile  << text << "$"
          << tp << " "
          << numEx << "\n";

  for (size_t i = 1; i <= numEx; ++i) {
    ex = static_cast<Exercise*>(exercises->removeNo(i));
    ex->toFile(toFile);
      exercises->add(ex);
  }
}


void Sport::edit(Sports* check) {
  char name[STRLEN];

    std::cout << "New name: ";
    Helper::input.get(name, "Invalid name!");

    if (!check->existing(name)) {
      strcpy(text, name);
    } else std::cout << "\n\tName is already registered\n";
}


bool Sport::editExercise(size_t ID) {
  if (Exercise *ex = static_cast<Exercise*>(exercises->remove(ID))) {
    ex->edit();
    exercises->add(ex);
    return true;
  }
  std::cout << "\n\tExercise not registered\n";
  return false;
}


void Sport::registerExercise(size_t ID) {
  if (exercises->noOfElements() < 20) {
    if (!exercises->inList(ID)) {
      exercises->add( new Exercise(ID) );
    } else std::cout << "\n\tID (" << ID << ") already used!\n";
  } else std::cout << "\n\tSport (" << text << ") has too many exercise entries!\n";
}


Exercise* Sport::removeExercise(std::size_t id) {
  return static_cast<Exercise*>(exercises->remove(id));
}

};
