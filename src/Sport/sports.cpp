#include <Sports.h>

namespace ooprog {


Sports::Sports() {
  sports = new List(Sorted);
}


Sports::~Sports() {
  delete sports;
}


void Sports::display() {
  sports->displayList();
}


void Sports::createMenu(Menu &menu) {
  ooprog::MenuItem *gren = menu.registerMenuItem(3, 'G', "Gren");
    gren->registerMenuItem(1, 'N', "Registrer en ny gren", std::bind(&Sports::registerSport, this));
    gren->registerMenuItem(2, 'E', "Endre en gren", std::bind(&Sports::editSport, this));
    gren->registerMenuItem(3, 'A', "Skriv hoveddataene om alle grener", std::bind(&Sports::display, this));
    gren->registerMenuItem(4, 'S', "Skriv alle data om en gitt gren", std::bind(&Sports::displayOne, this));


    //gren->registerMenuItem(5, 'Q', "Tilbake", gren->back);
}


void Sports::writeFile() {
  std::ofstream toFile("./data/sports.dta", std::ios::trunc);

  if (toFile) {
    Sport* sport;
    size_t numSports = sports->noOfElements();

    toFile << numSports << "\n";

    for (size_t i = 1; i <= numSports; ++i) {
    sport = static_cast<Sport*>(sports->removeNo(i));
    sport->toFile(toFile);
      sports->add(sport);
    }
  } else std::cout << "\n\tSport not registered\n";
}


void Sports::readFile() {
  std::ifstream fromFile("./data/sports.dta");

  if (fromFile) {
    std::string name;
    size_t numSports;
    fromFile >> numSports; fromFile.ignore();

    for (size_t i = 0; i < numSports; ++i) {
      std::getline(fromFile, name, '$');
      sports->add( new Sport(name, fromFile) );
    }
  } else std::cout << "\n\tCould not read from file sports.dta!\n";
}


void Sports::registerSport() {
  std::string name;

  std::cout << "\nEnter sport name: ";
    Helper::input.getline(name, "\n\tInvalid name!");

  if (!sports->inList(name.c_str())) {
    sports->add( new Sport(name) );
    writeFile();
  } else std::cout << "\n\tSport already registred \n";
}


void Sports::editSport() {
  std::string name;

  std::cout << "\nEnter sport name: ";
    Helper::input.getline(name, "\n\tInvalid name!");

  if (Sport* sport = static_cast<Sport*>(sports->remove(name.c_str()))) {
    sport->edit(this);
    sports->add(sport);
    writeFile();
  } else std::cout << "\n\tSport not registered\n";
}


void Sports::displayOne() {
  std::string name;

  std::cout << "Which sport?: " << '\n';
    Helper::input.getline(name, "\n\tInvalid name!");

  if (Sport* sport = static_cast<Sport*>(sports->remove(name.c_str()))) {
    sport->displayAll();
    sports->add(sport);
  } else std::cout << "\n\tSport not registered\n";
}


bool Sports::existing(std::string name) {
  return sports->inList(name.c_str());
}


Sport* Sports::getSport(std::string name) {
  Sport* sport = nullptr;

  if (sport = static_cast<Sport*>(sports->remove(name.c_str())))
    sports->add(sport);

  return sport;
}

};
