#include <Points.h>


namespace ooprog {

Points::Points() {
}

Points::~Points() {
}

void Points::readFromFile() {
  std::ifstream pointsFile("data/points.dta");
  for (size_t i = 0; i < nationlist.size(); i++) {
    addNationPoints(pointsFile);
  }
}

void Points::addNationPoints(std::ifstream &pointFile) {
  std::size_t points;
  pointFile >> points;
  pointslist.push_back(points);
}

void Points::display() {
  stats.importNations(nationlist);
  readFromFile();
  for (size_t i = 0; i < nationlist.size(); i++) {
    std::cout << nationlist.at(i) << " ";
    std::cout << " Poeng: " << pointslist.at(i) << std::endl;
  }
  pointslist.clear();
}


void Points::createMenu(Menu &menu) {
  menu.registerMenuItem(6, 'P', "Poengoversikt", std::bind(&Points::display, this));
}

// void Points::sortList() {
//   std::sort(medals.begin(), medals.end());
// }

};
