#include <iostream>
#include <string>

#include <Menu.h>
#include <MenuItem.h>

#include <Nations.h>
#include <Exercise.h>
#include <Participants.h>
#include <Sports.h>
#include <Points.h>
#include <Medals.h>
#include <Exercises.h>
bool running = true;


void close() {
  running = false;
}

ooprog::Nations *nations = new ooprog::Nations();
ooprog::Participants *participants = new ooprog::Participants();
ooprog::Sports *sports = new ooprog::Sports();
ooprog::Medals *medals = new ooprog::Medals();
ooprog::Points *points = new ooprog::Points();
ooprog::Exercises *exercises = new ooprog::Exercises();
ooprog::Stats stats;

void registerMenuEndpoints(ooprog::Menu &menu) {
  nations->createMenu(menu);
  participants->createMenu(menu);
  sports->createMenu(menu);

  medals->createMenu(menu);
  points->createMenu(menu);
  exercises->createMenu(menu);

  menu.registerMenuItem(7, 'X', "Exit / Avslutt", close);
}

void readFiles() {
  nations->readFile();
  participants->readFile();
  sports->readFile();
}



int main() {

  ooprog::Menu menu = ooprog::Menu("Main menu");
  registerMenuEndpoints(menu);

  readFiles();

  while(running) {
    menu.display();
  }

  //delete menu;
  return 0;
}
