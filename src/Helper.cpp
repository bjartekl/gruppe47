#include <Helper.h>

namespace ooprog {

  Helper::Input Helper::input = Helper::Input();
  Helper::Out Helper::out = Helper::Out();

  Helper::Helper() {

  }
  Helper::~Helper() {

  }

  Helper::Out &Helper::Out::operator<<(const char * token) {
    std::cout << token << std::endl;
    return *this;
  }

  bool Helper::fileExists(std::string fileName) {
    std::ifstream infile(fileName);
    return infile.good();
  }
  bool Helper::removeFile(std::string fileName) {
    std::string testFile = fileName;
    return !remove(testFile.c_str());
  }

  std::size_t Helper::indexOf(std::vector<std::size_t> list, std::size_t num) {
    for (size_t i = 0; i < list.size(); i++) {
      if(list.at(i) == num) return i;
    }
    return -1;
  }
}
