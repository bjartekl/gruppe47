#ifndef PARTICIPANTS_H
#define PARTICIPANTS_H

#include <string>
#include <fstream>
#include <ListTool2B.h>
#include <Menu.h>
#include <MenuItem.h>
#include <Helper.h>
#include <Nations.h>
#include <Participant.h>

extern ooprog::Nations *nations;

namespace ooprog {


class Participants {
private:

  /**
   * @variable participants
   * @type List pointer
  **/
  List *participants{};




public:

  /**
   * @method Participants
   * @type Constructor
   *
   * @return this
  **/
  Participants();

  /**
   * @method ~Participants
   * @type Destructor
  **/
  ~Participants();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  /**
   * @method registerParticipant
   * @description Register a new participant
   *
   * @type void
  **/
  void registerParticipant();

  /**
   * @method editParticipant
   * @description Edit a participant by their id
   *
   * @type void
  **/
  void editParticipant();

  /**
   * @method displayParticipants
   * @description Display all Participants
   *
   * @type void
  **/
  void displayParticipants();

  /**
   * @method displayParticipant
   * @description Display a specific participant
   *
   * @type void
  **/
  void displayParticipant();

  void displayParticipantById(std::size_t id);


  Participant *getParticipant(std::size_t id);
  /**
   * @method writeFile
   * @description Write all participants to a file
   *
   * @type void
  **/
  void writeFile();

  /**
   * @method readFile
   * @description Read all the participants from file
   *
   * @type void
  **/
  void readFile();

  std::string getParticipantNation(std::size_t id);

  void displayNationAthletes(std::string nation);
  /**
   * @method createMenu
   * @param [Menu&] menu, reference to the main menu
   * @description Create the participants menu
   *
   * @type void
  **/
  void createMenu(Menu &menu);
};

}

#endif
