#ifndef PARTICIPANT_H
#define PARTICIPANT_H

#include <ListTool2B.h>
#include <string>
#include <fstream>
#include <Nation.h>
#include <Helper.h>


namespace ooprog {

/**
 * @variable Sex
 * @type enum
**/
enum Sex : std::size_t;

class Participant : public NumElement {
private:


  std::size_t participantId{0};

  /**
   * @variable name
   * @type string
  **/
  std::string name;

  /**
   * @variable nation
   * @type string Country code
  **/
  std::string nation;

  /**
   * @variable sex
   * @type enum
  **/
  Sex sex;

public:

  /**
   * @method Participant
   * @type Constructor
   * @param [int] id, participant ID
   * @param [string] nationName, The country code of the nation
   *
   * @return this
  **/
  Participant(std::size_t id, std::string nationName);

  /**
   * @method Participant
   * @type Constructor
   * @param [int] id, participant ID
   * @param [ifstream &] file, The file containing participant data
   * @return this
  **/
  Participant(std::size_t id, std::ifstream &file);

  /**
   * @method ~Participant
   * @type Destructor
  **/
  ~Participant();


  void displayIfNation(std::string nationName);

  /**
   * @method edit
   * @description Edit a participant
   *
   * @type void
  **/
  void edit();

  /**
   * @method toFile
   * @description Write the participant to a file
   *
   * @type void
  **/
  void toFile(std::ofstream &file);


  std::string getNation();
  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();
};

}

#endif
