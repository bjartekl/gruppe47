#ifndef SPORT_H
#define SPORT_H

#include <iostream>
#include <string>
#include <cstring>
#include <ListTool2B.h>
#include <Exercise.h>
#include <Sports.h>
#include <Helper.h>
#include <Constants.h>
#include <Exercise.h>

namespace ooprog {

class Sports;

/**
 * @variable TP (time / points)
 * @type enum
**/
enum TP : std::size_t;

class Sport : public TextElement {
private:

  /**
   * @variable tp
   * @type enum
  **/
  TP tp;

  /**
   * @variable exercises
   * @type List pointer
  **/
  List *exercises{};

public:

  /**
   * @method Sport
   * @type Constructor
   *
   * @return this
  **/
  Sport(std::string name);

  /**
   * @method Sport
   * @type Constructor
   * @param [char*] name Unique sport name, injected into TextElement
   * @param [ifstream] fromFile Filestream with construction data
   *
   * @return this
  **/
  Sport(std::string name, std::ifstream & fromFile);

  /**
   * @method ~Sport
   * @type Destructor
  **/
  ~Sport();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  /**
   * @method displayExercises
   * @type function
  **/
  void displayExercises();

  /**
   * @method displayAll
   * @type function
  **/
  void displayAll();

  /**
   * @method toFile
   * @type function
   * @param [ofstream&] toFile Writes itself to toFile
  **/
  void toFile(std::ofstream &toFile);

  /**
   * @method edit
   * @type function
   * @param [Sports*] check is used for checking if the new name is used
  **/
  void edit(Sports* check);

  /**
   * @method editExercise
   * @type function
   * @param [int] ID used to find the Exercise in exercises list
  **/
  bool editExercise(std::size_t ID);

  /**
   * @method registerExercise
   * @type function
   * @param [size_t] Exercise ID
  **/
  void registerExercise(std::size_t ID);

  /**
   * @method removeExercise
   * @type function
   * @param [size_t] Exercise ID
  **/
  Exercise* removeExercise(std::size_t id);
};

}

#endif
