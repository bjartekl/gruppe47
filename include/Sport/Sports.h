#ifndef SPORTS_H
#define SPORTS_H

#include <ListTool2B.h>
#include <Menu.h>
#include <MenuItem.h>
#include <Sport.h>
#include <Helper.h>

namespace ooprog {

class Sport;

class Sports {
private:

  /**
   * @variable sports
   * @type List pointer
  **/
  List *sports{};

public:

  /**
   * @method Sports
   * @type Constructor
   *
   * @return this
  **/
  Sports();

  /**
   * @method ~Sports
   * @type Destructor
  **/
  ~Sports();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  /**
   * @method displayOne
   * @type function
  **/
  void displayOne();

  /**
   * @method createMenu
   * @type function
   * @param [Menu&] menu Menu is submenu of menu
  **/
  void createMenu(Menu &menu);

  /**
   * @method writeFile
   * @type function
  **/
  void writeFile();

  /**
   * @method readFile
   * @type function
  **/
  void readFile();

  /**
   * @method registerSport
   * @type function
  **/
  void registerSport();

  /**
   * @method editSport
   * @type function
  **/
  void editSport();

  /**
   * @method existing
   * @type function
   * @param [string] name for checking if new name is occupied
  **/
  bool existing(std::string name);

  /**
   * @method getSport
   * @type function
   * @param [string] name for checking ListTool
   * @param [string] pointer to be pointed
   *
   * return bool if the sport was found or not
  **/
  Sport* getSport(std::string name);
};

}

#endif
