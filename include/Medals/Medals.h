#ifndef MEDALS_H
#define MEDALS_H

#include <ListTool2B.h>
#include <Stats.h>
#include <Menu.h>
#include <fstream>
#include <vector>
#include <string>

extern ooprog::Stats stats;

namespace ooprog {

class Medals : public Stats {
private:

  struct medals {
    std::size_t gold{0};
    std::size_t silver{0};
    std::size_t bronze{0};

    void display() {
      std::cout << "Gold: " << gold << " "
                << "Silver: " << silver << " "
                << "Bronze: " << bronze << "\n";
    }
  };

  std::vector<medals> medalslist;

public:

  /**
   * @method Medals
   * @type Constructor
   *
   * @return this
  **/
  Medals();

  /**
   * @method ~Medals
   * @type Destructor
  **/
  ~Medals();


  void readFromFile();
  void addNationMedals(std::ifstream &medalFile);

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  void createMenu(Menu &menu);

  void addGold(std::size_t pos);
  void addSilver(std::size_t pos);
  void addBronze(std::size_t pos);
  void writeToFile();
  void sortList();
};

}

#endif
