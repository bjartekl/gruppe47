#ifndef NATION_H
#define NATION_H

#include <ListTool2B.h>
#include <string>
#include <iostream>
#include <fstream>
#include <Helper.h>
#include <Stats.h>

namespace ooprog {

class Nation : public TextElement {
private:

  /**
   * @variable name
   * @type string
  **/
  std::string name;

  /**
   * @variable numParticipants
   * @type int
  **/
  std::size_t numParticipants;

  /**
   * @variable contactName
   * @type string
  **/
  std::string contactName;

  /**
   * @variable contactCell
   * @type int
  **/
  std::size_t contactCell;


  // struct medals {
  //   std::size_t gold{0};
  //   std::size_t silver{0};
  //   std::size_t bronze{0};
  // };

public:

  /**
   * @method Nation
   * @type Constructor
   * @param [char*] ID Unique 3 char countrycode, injected into TextElement
   *
   * @return this
  **/
  Nation(char* ID);

  /**
   * @method Nation
   * @type Constructor
   * @param [char*] ID Unique 3 char countrycode, injected into TextElement
   * @param [ifstream] fromStream Filestream with construction data
   *
   * @return this
  **/
  Nation(char* ID, std::ifstream & fromFile);

  /**
   * @method ~Nation
   * @type Destructor
  **/
  ~Nation();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  /**
   * @method toFile
   * @type function
   * @param [ofstream&] toFile Writes itself to toFile
  **/
  void toFile(std::ofstream & toFile);

  /**
   * @method getName
   * @type function
   *
   * @return string
  **/
  std::string getName();

  /**
   * @method getId
   * @type function
   *
   * @return string
  **/
  std::string getId();

  /**
   * @method edit
   * @type function
  **/
  void edit();


  std::string getNation();

  void plusParticipantCount();
};

}

#endif
