#ifndef NATIONS_H
#define NATIONS_H

#include <string>
#include <cstring>
#include <ListTool2B.h>
#include <Menu.h>
#include <MenuItem.h>
#include <Nation.h>
#include <fstream>
#include <Constants.h>
#include <cstring>
#include <Helper.h>

extern ooprog::Stats stats;

namespace ooprog {
class Participants;


class Nations {
private:

  /**
   * @variable nations
   * @type List pointer
  **/
  List *nations{};

public:

  /**
   * @method Nations
   * @type Constructor
   *
   * @return this
  **/
  Nations();

  /**
   * @method ~Nations
   * @type Destructor
  **/
  ~Nations();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  /**
   * @method createMenu
   * @type function
   * @param [Menu&] menu Menu is submenu of menu
  **/
  void createMenu(Menu &menu);

  /**
   * @method writeFile
   * @type function
  **/
  void writeFile();

  /**
   * @method readFile
   * @type function
  **/
  void readFile();

  /**
   * @method registerNation
   * @type function
  **/
  void registerNation();


  void displayNationAthletes();

  void displayAll();

  void displayNation(std::string nationName);

  /**
   * @method editNation
   * @type function
  **/
  void editNation();

  bool nationExists(std::string nationName);

  void plusParticipantCount(std::string nationName);
};

}

#endif
