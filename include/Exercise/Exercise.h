#ifndef EXERCISE_H
#define EXERCISE_H

#include <ListTool2B.h>
#include <Participant.h>
#include <Menu.h>
#include <MenuItem.h>
#include <fstream>


namespace ooprog {

class Exercise : public NumElement {
private:

  /**
   * @variable name
   * @type string
  **/
  std::string name;

  /**
   * @variable time
   * @type int
  **/
  size_t time;

  /**
   * @variable date
   * @type int
  **/
  size_t date;

  /**
   * @variable numParticipants
   * @type int
  **/
  size_t numParticipants;

  /**
   * @variable result
   * @type Participant pointer
  **/
  Participant* results[];

public:

  /**
   * @method Exercise
   * @type Constructor
   * @param [int] ID the ID of the exercise
   *
   * @return this
  **/
  Exercise(size_t ID);

  /**
   * @method Exercise
   * @type Constructor
   * @param [int] ID the ID of the exercise
   * @param [ofstream&] fromFIle file with info to constructor
   *
   * @return this
  **/
  Exercise(size_t ID, std::ifstream & fromFile);

  /**
   * @method ~Exercise
   * @type Destructor
  **/
  ~Exercise();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();


  static void createMenu(Menu &menu);

  /**
   * @method toFile
   * @type function
   * @param [ofstream&] toFile Writes itself to toFile
  **/
  void toFile(std::ofstream & toFile);

  /**
   * @method edit
   * @type function
  **/
  void edit();

};

}

#endif
