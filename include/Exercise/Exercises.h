#ifndef EXERCISES_H
#define EXERCISES_H

//#include <ListTool2B.h>
#include <Menu.h>
#include <MenuItem.h>
#include <string>
#include <cstring>
#include <Helper.h>
#include <Sports.h>
#include <Sport.h>
#include <ResultList.h>
#include <StartList.h>
#include <Constants.h>


extern ooprog::Sports* sports;

namespace ooprog {

class Exercises {
private:

  std::string activeSport{};

  std::size_t activeId{0};

  Sport* sport{};
public:

  /**
   * @method Exercise
   * @type Constructor
   *
   * @return this
  **/
  Exercises();
  ~Exercises();

  void display();

  void createMenu(Menu &menu);
  void displaySubMenu();

  void writeStartList();
  void removeStartList();
  void createStartList();
  void editStartList();

  void writeResultList();
  void createResultList();
  void removeResultList();

  void registerExercise();

  void getName();
  void getId();
  void reset();
  void editExercise();
  void removeExercise();
};

}

#endif
