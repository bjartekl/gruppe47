#ifndef POINTS_H
#define POINTS_H

#include <ListTool2B.h>
#include <Stats.h>
#include <Menu.h>
#include <fstream>

extern ooprog::Stats stats;

namespace ooprog {

class Points : public Stats {
private:

  std::vector<std::size_t> pointslist;

public:

  /**
   * @method Points
   * @type Constructor
   *
   * @return this
  **/
  Points();

  /**
   * @method ~Points
   * @type Destructor
  **/
  ~Points();

  void readFromFile();
  void addNationPoints(std::ifstream &pointsFile);

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  void createMenu(Menu &menu);

  //void sortList();

};

}

#endif
