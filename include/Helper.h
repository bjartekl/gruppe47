#ifndef HELPER_H
#define HELPER_H
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <stdio.h>
#include <vector>

namespace ooprog {

class Helper {

private:

public:
  Helper();
  ~Helper();

  struct Out {
    Out & operator<< (const char * token);
  };

  struct Input {
    template <typename T>
    static void get(T &value, const char * errorMessage){
      while(!(std::cin >> value)) {
        std::cin.clear();
        while(std::cin.get() != '\n') continue;
        std::cout << errorMessage << std::endl;
      }
    }
    template <typename T, std::size_t size>
    static void get(T (&value)[size], const char * errorMessage){
      while(!(std::cin >> value) || std::strlen(value) > size) {
        std::cin.clear();
        while(std::cin.get() != '\n') continue;
        std::cout << errorMessage << std::endl;
      }
    }
    static std::size_t get(std::size_t from, std::size_t to, const char * errorMessage) {
      std::size_t num;
      get(num, "Value must be type number");
      while(num > to || num < from) {
        std::cout << errorMessage << std::endl;
        get(num, "Value must be type number");
      }
      return num;
    }
    template <typename T>
    static void getline(T &value, const char * errorMessage){
      //std::cin.ignore();
      while(!(std::getline(std::cin, value))) {
        std::cout << errorMessage << std::endl;
      }
    }
    template <typename T, std::size_t size>
    static void getline(T (&value)[size], const char * errorMessage){
      std::string tmp;
      std::getline(std::cin, tmp);
      while(tmp.length() > size) {
        std::cout << errorMessage << std::endl;
        std::getline(std::cin, tmp);
      }
      std::strcpy(value, tmp.c_str());
    }
  };
  static std::size_t indexOf(std::vector<std::size_t> list, std::size_t num);
  static bool fileExists(std::string filename);
  static bool removeFile(std::string fileName);
    static Input input;
    static Out out;
};

}


#endif;
