#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace ooprog {

  const size_t MAXNATIONS             = 200;
  const size_t MAXEXERCISES           =  20;
  const size_t MAXPARTICIPANTS        =  20;
  const size_t MAXPARTICIPANTSINLISTS = 500;
  const size_t STRLEN                 = 100;

};

#endif
