#ifndef STARTLIST_H
#define STARTLIST_H

#include <string>
#include <ListTool2B.h>
#include <Helper.h>
#include <fstream>
#include <Participants.h>
#include <Participant.h>
#include <vector>
#include <Menu.h>
#include <Constants.h>

extern ooprog::Participants *participants;
namespace ooprog {

class StartList {
private:

  std::size_t activeId{0};
  std::string activeSport{};
  std::string fileName{};

  std::vector<std::size_t> list;
  bool displayMenu{true};
  bool fileExists;
public:

  /**
   * @method StartList
   * @type Constructor
   *
   * @return this
  **/
  StartList();


  StartList(std::size_t id, std::string sport);

  /**
   * @method ~StartList
   * @type Destructor
  **/
  ~StartList();


  void remove();
  void createStartList();
  void editStartList();
  void addParticipantsToList();
  void showEditMenu();
  void parseFile();
  void editParticipantList();
  void removeParticipant();
  void exitMenu();
  void writeToFile();
  std::vector<std::size_t> getList();
  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();
};

}

#endif
