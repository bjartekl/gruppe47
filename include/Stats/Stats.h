#ifndef STATS_H
#define STATS_H

#include <iostream>
#include <ListTool2B.h>
#include <vector>
#include <string>


namespace ooprog {

class Stats {

protected:

  std::vector<std::string> nationlist;

public:

  /**
   * @method Stats
   * @type Constructor
   *
   * @return this
  **/
  Stats();

  /**
   * @method ~Stats
   * @type Destructor
  **/
  ~Stats();

  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();

  void importNations(std::vector<std::string> &tolist);

  void update(std::vector<std::string> &updatedNationlist);
};

}

#endif
