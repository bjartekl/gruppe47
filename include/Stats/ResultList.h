#ifndef RESULTLIST_H
#define RESULTLIST_H

#include <string>
#include <ListTool2B.h>
#include <Helper.h>
#include <fstream>
#include <Participants.h>
#include <StartList.h>
#include <vector>
#include <Medals.h>

extern ooprog::Stats stats;
extern ooprog::Participants *participants;
extern ooprog::Medals *medals;
namespace ooprog {

class ResultList {
private:

  std::size_t activeId{0};
  std::string activeSport{};
  std::string fileName{};
  std::vector<std::size_t> startList{};

  enum Status {FINISHED=0, BROKEOFF=1, NOSHOW=2, DISQUALIFIED=3};
  struct Result {
    Status status{NOSHOW};
    std::string time{"0"};
    std::size_t id{0};
    std::size_t startNr{0};
    std::size_t position{9999};
    bool operator<(const Result& a) const {
        return position < a.position;
    }
  };

  std::vector<Result> list;

public:

  /**
   * @method ResultList
   * @type Constructor
   *
   * @return this
  **/
  ResultList();


  ResultList(std::size_t id, std::string sport);

  /**
   * @method ~ResultList
   * @type Destructor
  **/
  ~ResultList();

  void remove();
  void parseFile(std::string file);
  void parsePerson(std::ifstream &file);
  void createResultList();
  void unfinished(std::size_t id, Status reason);
  void finishingNumber(std::size_t id);
  void writeToFile();
  void readFromFile();
  void updateMedals();
  /**
   * @method display
   * @type virtual function
  **/
  virtual void display();
};

}

#endif
