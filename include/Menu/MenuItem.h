#ifndef MENU_ITEM_H
#define MENU_ITEM_H
#include <ListTool2B.h>
#include <functional>
#include <iostream>
#include <cstring>

namespace ooprog {

class Menu;

class MenuItem : public NumElement  {
private:

  /**
   * @variable menuId
   * @type Int
  **/
  std::size_t menuId;

  /**
   * @variable menuKey
   * @type char
  **/
  char menuKey{};

  /**
   * @variable menuText
   * @type char pointer
  **/
  char *menuText{};

  Menu *previous{nullptr};
  /**
   * @variable callback
   * @type function pointer
  **/
  std::function<void()> callback{};

  /**
   * @variable subMenu
   * @type Menu pointer
  **/
  Menu *subMenu{nullptr};

  /**
   * @variable hasSubMenu
   * @type bool
  **/
  bool hasSubMenu{false};

public:

  /**
   * @method MenuItem
   * @type Constructor
   * @param [int] id The menu item's ID Used with List tools
   * @param [char] key The key required to active the callback
   * @param [char*] text Text displayed on the screen to the user
   * @param [function | optional] fn Callback used when menu item is activated
  **/
  MenuItem(std::size_t id, char key, char *text, Menu *prev,
           std::function<void()>fn = {});

  /**
   * @method ~MenuItem
   * @type [virtual] Destructor
  **/
  virtual ~MenuItem();

  /**
   * @method display
   * @type void
  **/
  virtual void display();

  /**
   * @method callIfMatch
   * @param [char] key runs the callback if key matches MenuItem's key
   * @type void
  **/
  void callIfMatch(char key);

  /**
   * @method registerMenuItem
   * @type function
   * @param [int] subId The menu item's subID Used with List tools
   * @param [char] key The key required to active the callback
   * @param [char*] text Text displayed on the screen to the user
   * @param [function | optional] fn Callback used when menu item is activated
   *
   * @return [MenuItem pointer] New item's pointer
  **/
  MenuItem *registerMenuItem(std::size_t subId, char key, char *text,
                             std::function<void()>fn = {});

  /**
   * @method getMenuId
   * @type size_t
   *
   * @return [int] current item's ID
  **/
  std::size_t getMenuId();

};

}

#endif
