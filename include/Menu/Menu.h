#ifndef MENU_H
#define MENU_H

#include <functional>
#include <ListTool2B.h>
#include <iostream>
#include <cstring>

namespace ooprog {
class MenuItem;

class Menu {
private:

  /**
   * @variable items
   * @type List pointer
  **/
  List *items{};

  /**
   * @variable name
   * @type char pointer
  **/
  char *name{};


  Menu *parent{nullptr};

public:
  /**
   * @method Menu
   * @type Constructor
   * @param [char *] n Menu title
   *
   * @return this
  **/
  Menu(char *n, Menu *ptr={nullptr});

  /**
   * @method ~Menu
   * @type Destructor
  **/
  ~Menu();

  /**
   * @method registerMenuItem
   * @type function
   * @param [int] id The menu item's ID Used with List tools
   * @param [char] key The key required to active the callback
   * @param [char*] text Text displayed on the screen to the user
   * @param [function | optional] fn Callback used when item is activated
   *
   * @return MenuItem Pointer
  **/
  MenuItem* registerMenuItem(std::size_t id, char key, char *text,
                             std::function<void()>fn = {});

  /**
   * @method getInput
   * @type function
   *
   * @return void
  **/
  void getInput();

  /**
   * @method parseInput
   * @type function
   * @param [char] key Looks for a menu item with desired key
   *
   * @return void
  **/
  void parseInput(char key) const;

  /**
   * @method display
   * @type function
   *
   * @return void
  **/
  void display();
};

}

#endif
