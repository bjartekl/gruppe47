CC=g++
CFLAGS= -std=c++11 -Wall -w -g -Wextra
INCLUDE:= -Iinclude/ -Iinclude/Exercise/ -Iinclude/Medals/ -Iinclude/Menu/ -Iinclude/Nation/ -Iinclude/Participant/ -Iinclude/Points/ -Iinclude/Sport/ -Iinclude/Stats/
SOURCE:= $(wildcard src/*.cpp) $(wildcard src/*/*.cpp)

EXE=main.out

run: clean $(EXE)

$(EXE):
	$(CC) $(CFLAGS) $(INCLUDE) $(SOURCE) -o $(EXE)

clean:
	rm -rf $(EXE)
	rm -rf main.out.dSYM
